package com.example.seataaccountservice2003.service.impl;

import com.example.seataaccountservice2003.dao.AccountDao;
import com.example.seataaccountservice2003.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    AccountDao accountDao;

    @Override
    public void decrease(Long userId, Double money) {
        int i=1/0; //此处认为制造异常，观察数据是否一致
        accountDao.del(userId, money);
    }
}
