package com.example.seataaccountservice2003.controller;

import com.example.seataaccountservice2003.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    @Autowired
    AccountService accountService;

    @GetMapping("/account/del")
    void del(Long userId,Double money){
        System.out.println(userId+" "+money);
        accountService.decrease(userId,money);
    }
}
