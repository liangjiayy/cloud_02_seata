package com.example.seataaccountservice2003.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AccountDao {
    void del(Long userId,Double money);
}
