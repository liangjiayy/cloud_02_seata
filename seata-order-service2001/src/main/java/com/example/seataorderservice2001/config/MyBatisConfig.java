package com.example.seataorderservice2001.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

//@Configuration
@MapperScan("com.example.seataorderservice2001.dao")
public class MyBatisConfig {

}