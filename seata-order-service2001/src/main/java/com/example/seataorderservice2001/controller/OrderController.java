package com.example.seataorderservice2001.controller;

import com.example.seataorderservice2001.domain.TOrder;
import com.example.seataorderservice2001.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/order/create")
    public String create(TOrder order){
        orderService.create(order);
        order.setStatus(1);
        return order+"已完成";
    }
}
