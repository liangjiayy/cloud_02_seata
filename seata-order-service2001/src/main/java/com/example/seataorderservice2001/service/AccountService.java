package com.example.seataorderservice2001.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "seata-account-service2003")
public interface AccountService {
    @GetMapping("/account/del")
    void del(@RequestParam(value = "userId") Long userId,
             @RequestParam(value = "money") Double money);
}
