package com.example.seataorderservice2001.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "seata-storage-service2002")
public interface StorageService {
    @GetMapping("/storage/del")
    void del(@RequestParam(value = "productId") Long productId, @RequestParam(value = "count") Long count);
}
