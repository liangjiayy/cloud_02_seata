package com.example.seataorderservice2001.service;

import com.example.seataorderservice2001.domain.TOrder;

public interface OrderService {
    //创建订单
    void create(TOrder order);
}
