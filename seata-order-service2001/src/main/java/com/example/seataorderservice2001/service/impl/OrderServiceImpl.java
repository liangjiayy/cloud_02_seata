package com.example.seataorderservice2001.service.impl;

import com.example.seataorderservice2001.dao.OrderDao;
import com.example.seataorderservice2001.domain.TOrder;
import com.example.seataorderservice2001.service.AccountService;
import com.example.seataorderservice2001.service.OrderService;
import com.example.seataorderservice2001.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderDao orderDao;

    @Autowired
    StorageService storageService;

    @Autowired
    AccountService accountService;

    @Override
    //@Transactional //在2003service层创造异常，检测数据库数据变化
    @GlobalTransactional(name = "create-order", rollbackFor = Exception.class)
    public void create(TOrder order) {
        ///创建订单
        log.info("创建订单");
        orderDao.create(order);

        ///商品数目减少
        log.info("商品数目减少");
        storageService.del(order.getProductId(),order.getCount());

        ///用户的钱减少
        log.info("用户的钱减少");
        accountService.del(order.getUserId(), order.getMoney());

        ///修改订单状态
        log.info("修改订单状态");
        orderDao.update(order.getUserId());
    }
}
