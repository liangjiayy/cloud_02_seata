package com.example.seataorderservice2001.dao;

import com.example.seataorderservice2001.domain.TOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderDao {
    //创建订单
    void create(TOrder order);
    //更新状态
    void update(Long userId);
}
