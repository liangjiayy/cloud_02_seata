package com.example.seatastorageservice2002.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

//@Configuration
@MapperScan("com.example.seatastorageservice2002.dao")
public class MyBatisConfig {

}