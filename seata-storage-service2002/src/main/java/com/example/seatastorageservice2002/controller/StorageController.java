package com.example.seatastorageservice2002.controller;

import com.example.seatastorageservice2002.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StorageController {
    @Autowired
    StorageService storageService;

    @GetMapping("/storage/del")
    void del(Long productId, Long count){
        storageService.del(productId, count);
    }
}
