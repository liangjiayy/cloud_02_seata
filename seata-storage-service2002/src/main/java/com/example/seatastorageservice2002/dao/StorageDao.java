package com.example.seatastorageservice2002.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StorageDao{
    //扣减库存
    void del(@Param("productId") Long productId,
             @Param("count") Long count);
}
