package com.example.seatastorageservice2002.service.impl;

import com.example.seatastorageservice2002.dao.StorageDao;
import com.example.seatastorageservice2002.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StorageServiceImpl implements StorageService {
    @Autowired
    StorageDao storageDao;

    @Override
    public void del(Long productId, Long count) {
        storageDao.del(productId, count);
    }
}
